export const USERS = [];

export const checkBody = (request, response, next) => {
  if (
    Object.keys(request.body).length === 0 &&
    Object.keys(request.params).length === 0
  ) {
    return response.json({ error: "empty body request" });
  }

  return next();
};

export const checkKeys = (request, response, next) => {
  if (request.method == "DELETE") {
    return next();
  }

  const correctKeys = ["name", "cpf"];

  const incomingKeys = Object.keys(request.body);

  const invalidKeys = [];

  if (String(incomingKeys) !== String(correctKeys)) {
    for (let key of incomingKeys) {
      if (!correctKeys.includes(key)) {
        invalidKeys.push(key);
      }
    }
    return response.status(404).json({ error: { invalidKeys } });
  }

  return next();
};

export const checkNameAndCpf = (request, response, next) => {
  let cpf = request.method === "POST" ? request.body.cpf : request.params.cpf;
  const name = request.body.name;

  if (request.method != "POST" || request.url.includes("notes")) {
    cpf = request.params.cpf;
  }

  const userIndex = USERS.findIndex((user) => user.cpf === cpf);

  if (userIndex == -1 && request.method != "POST") {
    return response.json({ error: "invalid cpf - user is not registered" });
  }

  if (cpf == undefined) {
    return response.status(404).json({ erro: "cpf is not defined" });
  }

  if (String(name) == String("")) {
    return response.status(404).json({ erro: "name is not defined" });
  }

  if (isNaN(cpf) || cpf.length != 11) {
    return response.status(404).json({
      error: "invalid cpf value",
      message: "cpf must be at least 11 characters",
    });
  }

  if (!isNaN(cpf)) {
    let cpfNumbers = cpf.split("");

    let sum = cpfNumbers.reduce(function (total, num) {
      return parseInt(total) + parseInt(num);
    });

    if (String(sum)[0] !== String(sum)[1]) {
      return response.status(404).json({
        error: "invalid cpf value",
        message: "sum of cpf numbers does not follow repition pattern",
      });
    }
  }

  if (userIndex != -1 && request.method === "POST") {
    return response.status(409).json({ error: "cpf already exists" });
  }

  return next();
};
