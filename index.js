import express from "express";
import { v4 as uuidv4 } from "uuid";
import { checkBody, checkKeys, checkNameAndCpf, USERS } from "./middlewares";

const app = express();

app.listen(3000);
app.use(express.json());

// CREATE USER ROUTE
app.post("/users", checkBody, checkKeys, checkNameAndCpf, (req, res) => {
  const { cpf, name } = req.body;

  const user = {
    id: uuidv4(),
    name: name,
    cpf: cpf,
    notes: [],
  };

  const userIndex = USERS.findIndex((user) => user.cpf === cpf);

  if (userIndex === -1) {
    USERS.push(user);
  }

  res.status(201).json(user);
});

// LIST USERS ROUTE
app.get("/users", (req, res) => {
  res.status(200).json(USERS);
});

// UPDATE USER ROUTE
app.patch("/users/:cpf", checkBody, checkKeys, checkNameAndCpf, (req, res) => {
  const { cpf } = req.params;

  const newUserInfo = {
    name: req.body.name,
    cpf: req.body.cpf,
  };

  const userIndex = USERS.findIndex((user) => user.cpf === cpf);

  if (userIndex !== -1) {
    USERS[userIndex] = { ...USERS[userIndex], ...newUserInfo };
  }

  res.json({
    message: "User is updated",
    users: [USERS[userIndex]],
  });
});

// DELETE USER ROUTE
app.delete("/users/:cpf", checkBody, checkKeys, checkNameAndCpf, (req, res) => {
  const { cpf } = req.params;

  const userIndex = USERS.findIndex((user) => user.cpf === cpf);

  USERS.splice(userIndex, 1);

  res.status(200).json({
    message: "User is deleted",
    users: USERS,
  });
});

// CREATE NOTE ROUTE
app.post("/users/:cpf/notes", (req, res) => {
  const { cpf } = req.params;
  const { title, content } = req.body;

  const user = USERS.find((user) => user.cpf == cpf);

  const post = {
    // MUDAR!
    id: 1,
    title,
    content,
    created_at: new Date(),
  };

  if (user) {
    user.notes.push(post);
    return res
      .status(201)
      .json({ message: `${post.title} was added into ${user.name}'s notes` });
  }

  res.json({ error: "user not found" });
});

// GET USER NOTES ROUTE
app.get("/users/:cpf/notes", (req, res) => {
  const { cpf } = req.params;

  const user = USERS.find((user) => user.cpf == cpf);

  if (!user) {
    return res.json({ error: "user is not registered" });
  }

  res.json(user.notes);
});

app.patch("/users/:cpf/notes/:id", (req, res) => {
  const { cpf, id } = req.params;

  const { title, content } = req.body;

  const user = USERS.find((user) => user.cpf == cpf);

  if (!user) {
    return res.json({ error: "user not found" });
  }

  const userIndex = USERS.findIndex((user) => user.cpf === cpf);
  const postIndex = user.notes.findIndex((note) => note.id == id);

  if (postIndex != -1 && (title || content)) {
    USERS[userIndex].notes[postIndex] = {
      ...USERS[userIndex].notes[postIndex],
      title,
      content,
      updated_at: new Date(),
    };

    return res.json([USERS[userIndex].notes[postIndex]]);
  } else {
    return res.json({ error: "note not found" });
  }
});

// DELETE NOTE ROUTE
app.delete("/users/:cpf/notes/:id", (req, res) => {
  const { cpf, id } = req.params;

  const userIndex = USERS.findIndex((user) => user.cpf == cpf);

  const noteIndex = USERS[userIndex].notes.findIndex((note) => note.id == id);

  if (userIndex != -1 && noteIndex != -1) {
    USERS[userIndex].notes.splice(noteIndex, 1);

    return res.json(USERS[userIndex]);
  }

  res.json({ error: "user or note not found" });
});
